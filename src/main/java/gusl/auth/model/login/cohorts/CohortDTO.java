package gusl.auth.model.login.cohorts;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CohortDTO {

    private String id;
    private String name;
    private String logo;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
