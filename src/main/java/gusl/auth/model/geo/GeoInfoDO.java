package gusl.auth.model.geo;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GeoInfoDO {

    private DeviceGeoLocationDataDO deviceInfo;
    private IpGeoLocationDataDO ipInfo;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
