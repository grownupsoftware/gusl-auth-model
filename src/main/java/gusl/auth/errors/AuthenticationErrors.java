package gusl.auth.errors;

import gusl.core.errors.ErrorDO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.model.exceptions.ValidationException;

public enum AuthenticationErrors {

    LOGIN_BARRED("AUTH01 [{0}] login barred [{1}] ", "login.barred"),
    UNAUTHORIZED("AUTH02 Not authorised", "server.error.not.authorised"),
    INVALID_TOKEN("AUTH03 Invalid token", "server.error.invalid.token"),
    SESSION_TOKEN_EXPIRED("AUTH04 Session Token Expired", "session.token.expired"),
    CUSTOMER_DENIED("AUTH05 Customer access is denied", "customer.access.denied"),
    EVENT_NOT_FOUND("AUTH06 Event [{0}] not found", "Event not found"),
    INVALID_VALUE("AUTH07 Invalid value, field {0} {1}", "invalid.value"),
    DAO_ERROR("AUTH08 Error DAO, index: {0} error: {1}", "dao.error"),
    USERNAME_NOT_FOUND("AUTH09 User not found by username {0}", "user.by.username.not.found"),
    USER_NOT_FOUND("AUTH09 User not found by id {0}", "user.by.id.not.found"),
    PARSE_ERROR("AUTH10 Failed to parse token", "failed.to.parse.token"),
    ERR_GOOGLE_TOKEN_IS_NULL("AUTH11 Google SSO token cannot be null", "google.sso.token.isnull"),
    ERR_GOOGLE_TOKEN_VALIDATION("AUTH12 Failed to validate Google SSO token", "google.sso.error.in.validation"),
    ERR_GOOGLE_TOKEN_VERIFY("AUTH13 Failed to verify Google SSO token", "google.sso.error.in.verify"),
    ERR_GOOGLE_TOKEN_INVALID_HOSTNAME("AUTH14 Invalid hostname [{0}] is Google SSO token", "google.sso.invalid.hostname"),
    ERR_GOOGLE_TOKEN_EXPIRED("AUTH15 Token expired", "google.sso.token.expired"),
    ERR_GOOGLE_TOKEN_UNKNOWN_EMAIL("AUTH16 Email [{0}] not registered - please ask administrator to add", "email.not.registered"),
    ERR_GOOGLE_TOKEN_ACCESS_DENIED("AUTH17 Access is denied for [{0}]- please speak to the system administrator", "email.access.denied"),
    ERR_GOOGLE_TOKEN_INVALID_ID("AUTH18 Invalid id [{0}] does not match [{1}] in Google SSO token", "google.sso.invalid.id"),
    ERR_GOOGLE_TOKEN_INVALID_EMAIL("AUTH19 Invalid email [{0}] does not match [{1}] in Google SSO token", "google.sso.invalid.email"),
    ERR_GOOGLE_TOKEN_INVALID_EMAIL_VALIDATION("AUTH10 Email [{0}] in Google SSO token failed validation", "google.sso.invalid.email.validation"),
    ERR_GOOGLE_TOKEN_INVALID_NAME("AUTH20 Invalid name [{0}] does not match [{1}] in Google SSO token", "google.sso.invalid.name"),
    ERR_LOGIN_FAILURE("AUTH21 Login authentication failed [{0}]", "login.failed"),
    INVALID_STATUS_CHANGE("AUTH22 Invalid status change from {0} to {1} ", "invalid.status.change"),
    USERNAME_EXISTS("AUTH23 Username already exists {0}", "username.exists"),
    FAILED_TO_HASH_PASSWORD("AUTH24 Failed to hash password for {0}", "password.hash.failed"),
    RECORD_NOT_FOUND("AUTH25 Record {0} in {1} not found", "record.not.found"),
    NO_PACKAGE_PREFIX_DEFINED("AUTH26 No package prefixes defined, call initialise('package')", "no.package.names.defined"),
    PASSWORD_CHANGE_REQUIRED("AUTH27 Password change required", "password change required"),
    TOTP_SETUP_REQUIRED("AUTH28 TOTP setup required", "totp.setup.required"),
    TOTP_ENTRY_REQUIRED("AUTH29 TOTP entry required", "totp.entry.required"),
    ZIP_FILE_HAS_NO_CONTENTS("BOE003 Zip file has no contents", "zip.file.no.contents"),
    FAILED_TO_UPLOAD_FILE("BOE004 Failed to upload promotion file", "failed.to.upload.file"),
    PASSWORDS_NO_MATCH("AUTH30 Passwords do not match", "passwords.no.match"),
    INVALID_PASSWORD("AUTH31 Invalid current password", "invalid.current.password");

    private final String message;
    private final String messageKey;

    AuthenticationErrors(String message, String messageKey) {
        this.message = message;
        this.messageKey = messageKey;
    }

    public ErrorDO getError() {
        return new ErrorDO(message, messageKey);
    }

    public ErrorDO getError(Long id) {
        if (id != null) {
            return new ErrorDO(null, message, messageKey, String.valueOf(id));
        } else {
            return new ErrorDO(message, messageKey);
        }
    }

    public ErrorDO getError(String... params) {
        if (params != null) {
            return new ErrorDO(null, message, messageKey, params);
        } else {
            return new ErrorDO(message, messageKey);
        }
    }

    public GUSLErrorException generateException(String... params) {
        return new GUSLErrorException(getError(params));
    }

    public GUSLErrorException generateException(boolean suppressStack, String... params) {
        return new GUSLErrorException(getError(params), suppressStack);
    }

    public ValidationException validationException(String... params) {
        return new ValidationException(getError(params));
    }

    public GUSLErrorException generateException(Throwable t, String... params) {
        return new GUSLErrorException(getError(params), t);
    }
}
