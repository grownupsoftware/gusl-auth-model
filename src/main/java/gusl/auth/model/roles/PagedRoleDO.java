package gusl.auth.model.roles;

import gusl.annotations.form.page.AbstractPageResponse;
import gusl.annotations.form.page.PageResponse;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class PagedRoleDO extends AbstractPageResponse implements PageResponse<RoleDO> {

    @ToStringCount
    private List<RoleDO> content;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
