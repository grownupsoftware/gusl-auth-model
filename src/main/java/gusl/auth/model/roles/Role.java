package gusl.auth.model.roles;

import gusl.model.Identifiable;

public interface Role extends Identifiable<String> {

    String getName();

    PermissionsDO getPermissions();

    String getId();

}
