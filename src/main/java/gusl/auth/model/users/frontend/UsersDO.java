package gusl.auth.model.users.frontend;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UsersDO {

    private List<UserDO> users;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
