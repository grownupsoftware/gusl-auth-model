package gusl.auth.model.security;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ScratchCodesDO {

    private List<ScratchCodeDO> codes;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
