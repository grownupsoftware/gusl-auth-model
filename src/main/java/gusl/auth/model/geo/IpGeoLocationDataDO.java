package gusl.auth.model.geo;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringIgnoreNull;
import lombok.*;

import java.net.InetAddress;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToStringIgnoreNull
public class IpGeoLocationDataDO {

    private String cityName;
    private String continentCode;
    private String countryCode;
    private String countryName;
    private String latitude;
    private String longitude;
    private String postalCode;
    private String regionName;
    private String timezone;
    private InetAddress ipAddress;
    private boolean localAddress;

    public String getIpAddressAsString() {
        if (ipAddress != null) {
            return ipAddress.getHostAddress();
        }

        return null;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
