package gusl.auth.model.session;

import gusl.model.StateTransitionValidator;

import static gusl.auth.model.session.SessionStatus.*;

public class SessionStatusTransitionValidator extends StateTransitionValidator<SessionStatus> {

    public SessionStatusTransitionValidator() {
        super();
    }

    @Override
    protected void populateTransitionsMap() {

        // -->  ACTIVE
        transitionsMap.put(ACTIVE, ACTIVE);
        transitionsMap.put(BLOCKED, ACTIVE);
        transitionsMap.put(LOGGED_OUT, ACTIVE);
        transitionsMap.put(EXPIRED, ACTIVE);

        // -->  BLOCKED
        transitionsMap.put(ACTIVE, BLOCKED);
        transitionsMap.put(BLOCKED, BLOCKED);
        transitionsMap.put(LOGGED_OUT, BLOCKED);
        transitionsMap.put(EXPIRED, BLOCKED);

        // -->  LOGGED_OUT
        transitionsMap.put(ACTIVE, LOGGED_OUT);
        transitionsMap.put(BLOCKED, LOGGED_OUT);
        transitionsMap.put(LOGGED_OUT, LOGGED_OUT);
        transitionsMap.put(EXPIRED, LOGGED_OUT);

        // -->  EXPIRED
        transitionsMap.put(ACTIVE, EXPIRED);
        transitionsMap.put(BLOCKED, EXPIRED);
        transitionsMap.put(LOGGED_OUT, EXPIRED);
        transitionsMap.put(EXPIRED, EXPIRED);

    }
}

