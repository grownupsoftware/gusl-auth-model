package gusl.auth.model.session;

import gusl.core.tostring.ToString;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class SessionSelectAllDO {

    private String bettorId;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
