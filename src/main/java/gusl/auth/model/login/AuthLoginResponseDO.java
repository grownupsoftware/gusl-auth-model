package gusl.auth.model.login;

import gusl.auth.model.session.CreateSessionResponseDO;
import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@NoArgsConstructor
@Getter
@Setter
public class AuthLoginResponseDO extends CreateSessionResponseDO {

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
