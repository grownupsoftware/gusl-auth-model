package gusl.auth.model.token;

import gusl.core.tostring.ToString;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserTokenDO extends AbstractBaseTokenDO {

    final private static String CLAIM_KEY_SECRET = "secretKey";

    private String username;
    private String secretKey;

    private boolean customer;
    private Date expiryDate;

    @Override
    public Claims getClaims() {
        Claims claims = Jwts.claims().setSubject(username);
        claims.setIssuedAt(createdDate);
        claims.setId(id);
        claims.setIssuedAt(getCreatedDate());
        claims.put(CLAIM_KEY_SECRET, secretKey);
        claims.setExpiration(expiryDate);
        return claims;
    }

    public static UserTokenDO parseClaims(Claims claims) {
        return UserTokenDO.builder()
                .username(claims.getSubject())
                .secretKey(claims.get(CLAIM_KEY_SECRET).toString())
                .expiryDate(claims.getExpiration())
                .createdDate(claims.getIssuedAt())
                .id(claims.getId())
                .build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
