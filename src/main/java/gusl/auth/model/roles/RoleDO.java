package gusl.auth.model.roles;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.elastic.model.ESType;
import gusl.elastic.model.ElasticField;
import gusl.elastic.model.WithConfirmation;
import gusl.model.Identifiable;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.ZonedDateTime;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "Role comprises a collection of permissions")
public class RoleDO implements Role, Identifiable<String>, WithConfirmation {

    @DocField(description = "Database version - used by Evolutions migrate tool")
    public static final int VERSION = 4;

    @PostgresField(type = PostgresFieldType.TEXT, primaryKey = true, notNull = true, populate = true)
    @ElasticField(type = ESType.KEYWORD, populate = true, primaryKey = true)
    @DocField(description = "Unique id")
    private String id;

    @DocField(description = "Cohort Id")
    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String cohortId;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "A simplified descriptive term for the role")
    private String code;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "Name / Description of role")
    private String name;

    @PostgresField(type = PostgresFieldType.JSONB)
    @ElasticField(type = ESType.OBJECT)
    @DocField(description = "A collection of permissions for this role")
    private PermissionsDO permissions;

    @ElasticField(type = ESType.DATE, dateCreated = true, populate = true)
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateCreated = true, populate = true)
    @DocField(description = "Date created")
    private ZonedDateTime dateCreated;

    @ElasticField(type = ESType.DATE, dateUpdated = true, populate = true)
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateUpdated = true, populate = true)
    @DocField(description = "Date last modified")
    private ZonedDateTime dateUpdated;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "Roles are by node types")
    private String nodeType;

}
