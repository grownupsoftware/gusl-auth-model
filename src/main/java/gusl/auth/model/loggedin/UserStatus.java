package gusl.auth.model.loggedin;

public enum UserStatus {
    AWAITING_EMAIL_VERIFICATION,
    AWAITING_KYC,
    ENABLED,
    DISABLED,
    BANNED;

    public boolean canLogin() {
        return this == ENABLED;
    }
}
