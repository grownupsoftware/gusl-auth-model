package gusl.auth.model.totp;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QrCodeResponseDTO {

    private String qrCode;

    public static QrCodeResponseDTO of(String qrCode) {
        return QrCodeResponseDTO.builder().qrCode(qrCode).build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
