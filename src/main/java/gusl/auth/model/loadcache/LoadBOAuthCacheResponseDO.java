package gusl.auth.model.loadcache;

import gusl.auth.model.roles.RoleDO;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import lombok.*;

import java.util.List;

import static java.util.Objects.isNull;

@NoArgsConstructor
@Getter
@Setter
@Builder
@AllArgsConstructor
public class LoadBOAuthCacheResponseDO {

//    @ToStringCount
//    private List<BackofficeUserDO> users;

    @ToStringCount
    private List<RoleDO> roles;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    public String pretty() {
        return String.format("%-20s %6s\n", "Cache", "Size") +
                String.format("%-20s %6s\n", "-----", "----") +
//                String.format("%-20s %6d\n", "admin users", size(users)) +
                String.format("%-20s %6d\n", "roles", size(roles));
    }

    private int size(List<?> list) {
        return isNull(list) ? 0 : list.size();
    }

}
