package gusl.auth.model.security;

import gusl.auth.model.roles.RolesIdsDO;
import gusl.core.annotations.DocField;
import gusl.core.security.ObfuscateMethod;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringMask;
import gusl.elastic.model.ESType;
import gusl.elastic.model.ElasticField;
import gusl.elastic.model.WithConfirmation;
import gusl.model.cohort.CohortIdsDO;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import gusl.postgres.model.PostgresIndex;
import lombok.*;

import java.time.ZonedDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserSecurityDO implements WithConfirmation {

    @DocField(description = "Database version - used by Evolutions migrate tool")
    public static final int VERSION = 15;

    @PostgresField(type = PostgresFieldType.TEXT, primaryKey = true, notNull = true)
    @ElasticField(type = ESType.KEYWORD, primaryKey = true)
    @DocField(description = "Unique Id (Must be the same as unique Id on user table)")
    private String id;

    @PostgresIndex(unique = true)
    @PostgresField(type = PostgresFieldType.TEXT, notNull = true)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "Username or email - must match username / email on User record")
    private String username;

    @ToStringMask
    @PostgresField(type = PostgresFieldType.TEXT, notNull = true, securityMethod = ObfuscateMethod.SALTED_WRITE)
    @ElasticField(type = ESType.KEYWORD, securityMethod = ObfuscateMethod.SALTED_WRITE)
    @DocField(description = "Salted user password - can be validated but never read")
    private String password;

    @ToStringMask
    @PostgresIndex
    @DocField(description = "JWT token used in auto-session renewal")
    @PostgresField(type = PostgresFieldType.TEXT, securityMethod = ObfuscateMethod.OBFUSCATED_WRITE)
    @ElasticField(type = ESType.KEYWORD, securityMethod = ObfuscateMethod.OBFUSCATED_WRITE)
    private String secretKey;

    @ToStringMask
    @PostgresIndex
    @PostgresField(type = PostgresFieldType.TEXT, securityMethod = ObfuscateMethod.OBFUSCATED_WRITE)
    @ElasticField(type = ESType.KEYWORD, securityMethod = ObfuscateMethod.OBFUSCATED_WRITE)
    @DocField(description = "Unique user key for Google Authenticator")
    private String authenticatorKey;

    @DocField(description = "Google Authenticator's set of backup  / scratch codes")
    private ScratchCodesDO scratchCodes;

    @ElasticField(type = ESType.DATE, dateCreated = true, populate = true)
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateCreated = true, populate = true)
    @DocField(description = "Date record created")
    private ZonedDateTime dateCreated;

    @ElasticField(type = ESType.DATE, dateUpdated = true, populate = true)
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateUpdated = true, populate = true)
    @DocField(description = "Date record modified")
    private ZonedDateTime dateUpdated;

    @PostgresField(type = PostgresFieldType.JSONB)
    @ElasticField(type = ESType.OBJECT)
    @DocField(description = "List of role ids that corresponds to the unique id sin teh Role table")
    private RolesIdsDO roleIds;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    // JWT Token used as magic link for email validation
    @DocField(description = "JWT token used as the magic link in email validation link.")
    private String validationKey;

    @PostgresField(type = PostgresFieldType.BOOLEAN)
    @ElasticField(type = ESType.BOOLEAN)
    @DocField(description = "Boolean flag set when email has been validated")
    private Boolean emailValidated;

    @PostgresField(type = PostgresFieldType.JSONB)
    @ElasticField(type = ESType.OBJECT)
    @DocField(description = "Allowed access to these cohorts")
    private CohortIdsDO allowedCohorts;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "Current selected cohort")
    private String currentCohortId;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "Associated telegram id of user")
    private String telegramId;

    @PostgresField(type = PostgresFieldType.BOOLEAN)
    @ElasticField(type = ESType.BOOLEAN)
    @DocField(description = "User requires One Time Password - overrides cohort and system cache")
    private Boolean requiresTOTP;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
