package gusl.auth.model.geo;

import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DeviceGeoLocationDataDO {

    @DocField(description = "True if device location received")
    private boolean present;

    @DocField(description = "Latitude of the device")
    private Double latitude;

    @DocField(description = "Longitude of the device")
    private Double longitude;

    @DocField(description = "Accuracy to 95% confidence")
    private Double accuracy;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
