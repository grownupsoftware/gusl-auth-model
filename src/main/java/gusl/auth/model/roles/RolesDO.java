package gusl.auth.model.roles;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class RolesDO {

    @ToStringCount
    @Singular
    private List<RoleDO> roles;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
