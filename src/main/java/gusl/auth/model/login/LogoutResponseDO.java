package gusl.auth.model.login;

import gusl.core.tostring.ToString;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
//@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LogoutResponseDO {

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
