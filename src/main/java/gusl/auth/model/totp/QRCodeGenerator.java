package gusl.auth.model.totp;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitArray;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.warrenstrange.googleauth.GoogleAuthenticatorConfig;
import com.warrenstrange.googleauth.HmacHashFunction;
import gusl.auth.model.security.UserSecurityDO;
import gusl.auth.model.users.frontend.UserDO;
import gusl.core.exceptions.GUSLException;
import gusl.core.json.JsonUtils;
import gusl.core.security.ObfuscatedStorage;
import gusl.model.errors.SystemErrors;
import gusl.model.nodeconfig.GoogleAuthenticatorConfigDO;
import gusl.model.nodeconfig.NodeConfig;
import gusl.node.properties.GUSLConfigurable;
import lombok.CustomLog;
import org.apache.http.client.utils.URIBuilder;

import javax.inject.Singleton;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.isNull;

@Singleton
@CustomLog
public class QRCodeGenerator implements GUSLConfigurable {

    private GoogleAuthenticatorConfigDO theGoogleAuthenticatorConfig;

    public String getOtpAuthURL(UserDO user, UserSecurityDO userSecurity) {
        return String.format(theGoogleAuthenticatorConfig.getUriFormat(), internalURLEncode(getOtpAuthTotpURL(user, userSecurity)));
    }

    public String getOtpAuthURL(String username, String authenticatorKey) {
        return String.format(theGoogleAuthenticatorConfig.getUriFormat(), internalURLEncode(getOtpAuthTotpURL(username, authenticatorKey)));
    }

    private String getOtpAuthTotpURL(UserDO user, UserSecurityDO userSecurity) {
        return getOtpAuthTotpURL(user.getUsername(), userSecurity.getAuthenticatorKey());
    }

    private String getOtpAuthTotpURL(String username, String authenticatorKey) {
        String secretKey = ObfuscatedStorage.isObfuscated(authenticatorKey) ? ObfuscatedStorage.decryptKey(authenticatorKey) : authenticatorKey;

        URIBuilder uri = new URIBuilder()
                .setScheme("otpauth")
                .setHost("totp")
                .setPath("/" + theGoogleAuthenticatorConfig.getIssuer() + ":" + username)
                .setParameter("secret", secretKey)
                .setParameter("issuer", theGoogleAuthenticatorConfig.getIssuer());

        GoogleAuthenticatorConfig config = new GoogleAuthenticatorConfig();
        uri.setParameter("algorithm", getAlgorithmName(config.getHmacHashFunction()));
        uri.setParameter("digits", String.valueOf(config.getCodeDigits()));
        uri.setParameter("period", String.valueOf((int) (config.getTimeStepSizeInMillis() / 1000)));

        return uri.toString();
    }

    private String internalURLEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException var2) {
            throw new RuntimeException("UTF-8 encoding is not supported by URLEncoder.", var2);
        }
    }

    private String getAlgorithmName(HmacHashFunction hashFunction) {
        switch (hashFunction) {
            case HmacSHA1:
                return "SHA1";

            case HmacSHA256:
                return "SHA256";

            case HmacSHA512:
                return "SHA512";

            default:
                throw new IllegalArgumentException(String.format("Unknown algorithm %s", hashFunction));
        }
    }

    public String generateQRSVG(UserDO user, UserSecurityDO userSecurity) throws WriterException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        Map<EncodeHintType, Object> hints = new HashMap<>();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        hints.put(EncodeHintType.CHARACTER_SET, StandardCharsets.UTF_8.name());

        BitMatrix bitMatrix = qrCodeWriter.encode(getOtpAuthTotpURL(user, userSecurity), BarcodeFormat.QR_CODE, 100, 100, hints);

        StringBuilder sbPath = new StringBuilder();
        int width = bitMatrix.getWidth();
        int height = bitMatrix.getHeight();

        BitArray row = new BitArray(width);
        for (int y = 0; y < height; ++y) {
            row = bitMatrix.getRow(y, row);
            for (int x = 0; x < width; ++x) {
                if (row.get(x)) {
                    sbPath.append(" M" + x + "," + y + "h1v1h-1z");
                }
            }
        }

        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        sb.append("<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 ").append(width).append(" ").append(height).append("\" stroke=\"none\">\n");
//        sb.append("<style type=\"text/css\">\n");
        sb.append(".black {fill:#000000;}\n");
        sb.append("</style>\n");
        sb.append("<path class=\"black\"  d=\"").append(sbPath).append("\"/>\n");
        sb.append("</svg>\n");

        return sb.toString();
    }

    @Override
    public void configure(NodeConfig nodeConfig) throws GUSLException {
        theGoogleAuthenticatorConfig = nodeConfig.getGoogleAuthenticatorConfig();
        if (isNull(theGoogleAuthenticatorConfig)) {
            logger.debug("No Google Authenticator Config: {}", JsonUtils.prettyPrint(nodeConfig));
            // throw SystemErrors.INVALID_VALUE.generateException("No Google Authenticator Config in application.json");
        }
    }
}
