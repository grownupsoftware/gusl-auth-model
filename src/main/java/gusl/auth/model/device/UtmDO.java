package gusl.auth.model.device;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UtmDO {

    private String utmSource;
    private String utmMedium;
    private String utmCampaign;
    private String utmTerm;
    private String utmContent;
    private String utmOther;
    private String utmSet;

    public static UtmDO blank() {
        return UtmDO.builder()
                .utmCampaign("")
                .utmContent("")
                .utmMedium("")
                .utmOther("")
                .utmSet("")
                .utmSource("")
                .utmTerm("")
                .utmSet("")
                .build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
