package gusl.auth.model.totp;

import gusl.core.tostring.ToString;
import lombok.*;

import javax.validation.constraints.NotNull;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QrCodeRequestDTO {

    @NotNull
    private String username;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
