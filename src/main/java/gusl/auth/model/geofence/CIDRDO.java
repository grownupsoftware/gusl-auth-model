package gusl.auth.model.geofence;

import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import javax.validation.constraints.NotNull;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CIDRDO {

    @NotNull
    @UiField(type = FieldType.text, label = "CIDR", displayInTable = true)
    private String cidr;

    @NotNull
    @UiField(type = FieldType.text, label = "Network", displayInTable = true)
    private String name;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
