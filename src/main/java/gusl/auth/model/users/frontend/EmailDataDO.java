package gusl.auth.model.users.frontend;

import gusl.auth.model.security.UserSecurityDO;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class EmailDataDO {
    private String publicUrl;
    private UserDO user;
    private UserSecurityDO security;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
