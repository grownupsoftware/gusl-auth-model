package gusl.auth.model.geofence;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GeoFencesDO {

    @ToStringCount
    @Singular
    private List<GeoFenceDO> geoFences;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
