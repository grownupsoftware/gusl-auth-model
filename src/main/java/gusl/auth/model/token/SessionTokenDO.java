/* Copyright lottomart */
package gusl.auth.model.token;

import gusl.core.tostring.ToString;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SessionTokenDO extends AbstractBaseTokenDO {

    private String username;
    private String id;
    private String sessionId;
    private Date expiryDate;

    @Override
    public Claims getClaims() {
        Claims claims = Jwts.claims().setSubject(username);
        claims.setIssuedAt(getCreatedDate());
        claims.setId(String.valueOf(getId()));
        claims.setExpiration(expiryDate);
        claims.put("sessionId", sessionId);

        return claims;
    }

    public static SessionTokenDO parseClaims(Claims claims) {
        return SessionTokenDO.builder()
                .id(claims.getId())
                .username(claims.getSubject())
                .expiryDate(claims.getExpiration())
                .sessionId((String) claims.get("sessionId"))
                .createdDate(claims.getIssuedAt())
                .build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
