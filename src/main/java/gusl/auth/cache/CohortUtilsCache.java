package gusl.auth.cache;

import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.auth.model.login.cohorts.CohortDTO;
import gusl.query.QueryParams;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface CohortUtilsCache {

    String ALL_USER_ALLOWED_COHORTS = "ALL_ALLOWED";

    String COHORT_LOOKUP_URL = "/cohort/lookup";

    String getDefaultBOCohortId();

    String getDefaultFECohortId();

    void addCohortToQueryParams(final LoggedInUserDO loggedInUser, String defaultCohortId, final QueryParams params);

    void addCohortToQueryParamsForFE(final LoggedInUserDO loggedInUser, QueryParams params);

    void addCohortToQueryParamsForBO(final LoggedInUserDO loggedInUser, QueryParams params);

    String getCohortLogo(String cohortId);

    CohortDTO getCohortDTO(String cohortId);

    CohortLookupBO getCohortLookup(String cohortId);
}
