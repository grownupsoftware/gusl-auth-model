package gusl.auth.model.login;

import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TokenLoginRequestDO extends SessionDataDO {

    private String token; // google
    private String userToken;
    private String sessionToken;
    private String apiId;
    private String apiPassword;

    private String msalUsername; // MSAL
    private String msalToken;
    private String msalName; // can be removed if we get name in payload


    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
