package gusl.auth.model.users.frontend;

import gusl.annotations.form.page.AbstractPageResponse;
import gusl.annotations.form.page.PageResponse;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class PagedUserDO extends AbstractPageResponse implements PageResponse<UserDO> {

    @ToStringCount
    private List<UserDO> content;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
