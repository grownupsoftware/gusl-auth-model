/* Copyright lottomart */
package gusl.auth.model.token;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.UriInfo;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "Application Request Context")
public class ApplicationRequestContextDO {

    @DocField(description = "Unique Request Id")
    private Long requestId;

    @DocField(description = "Associated async response")
    private AsyncResponse asyncResponse;

    @DocField(description = "HTTP Servlet RequestF")
    private HttpServletRequest httpServletRequest;

    @DocField(description = "URI Info")
    private UriInfo uriInfo;

    @DocField(description = "HTTP Servlet Response")
    private HttpServletResponse httpServletResponse;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
