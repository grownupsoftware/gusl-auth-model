/* Copyright lottomart */
package gusl.auth.model.token;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AnalyticsTokenDO extends AbstractBaseTokenDO {

    private String uuid;

    @Override
    public Claims getClaims() {
        Claims claims = Jwts.claims().setSubject("analytics_" + getId());
        claims.setIssuedAt(getCreatedDate());
        claims.setId(id);
        claims.setIssuedAt(getCreatedDate());
        return claims;
    }

    public static AnalyticsTokenDO parseClaims(Claims claims) {
        AnalyticsTokenDO analyticsTokenDO = new AnalyticsTokenDO();
        analyticsTokenDO.setId(claims.getId());
        analyticsTokenDO.setCreatedDate(claims.getIssuedAt());

        return analyticsTokenDO;

    }

}
