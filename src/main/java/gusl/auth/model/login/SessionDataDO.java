package gusl.auth.model.login;

import gusl.auth.model.device.DeviceInfoDO;
import gusl.auth.model.device.UtmDO;
import gusl.auth.model.geo.DeviceGeoLocationDataDO;
import gusl.auth.model.geo.IpGeoLocationDataDO;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import gusl.elastic.model.ESType;
import gusl.elastic.model.ElasticField;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Map;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SessionDataDO {

    @PostgresField(type = PostgresFieldType.JSONB)
    @ElasticField(type = ESType.OBJECT)
    private Map<String, String> headers;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String clientIp;

    @PostgresField(type = PostgresFieldType.JSONB)
    @ElasticField(type = ESType.OBJECT)
    private IpGeoLocationDataDO ipLocationData;

    @PostgresField(type = PostgresFieldType.JSONB)
    @ElasticField(type = ESType.OBJECT)
    private DeviceGeoLocationDataDO deviceLocationData;

    @PostgresField(type = PostgresFieldType.JSONB)
    @ElasticField(type = ESType.OBJECT)
    private DeviceInfoDO deviceInfo;

    @PostgresField(type = PostgresFieldType.JSONB)
    @ElasticField(type = ESType.OBJECT)
    private UtmDO utm;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "Timezone sent with browser request. Expiry time on MSAL token is based on browser time zone")
    private String timezone;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
