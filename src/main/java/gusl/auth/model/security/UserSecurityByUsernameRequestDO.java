package gusl.auth.model.security;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class UserSecurityByUsernameRequestDO {

    private String username;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
