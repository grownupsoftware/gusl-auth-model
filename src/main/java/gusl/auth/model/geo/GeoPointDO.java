package gusl.auth.model.geo;

import gusl.core.annotations.DocClass;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "Lat / Long Geo Point (compatible with ElasticSearch)  ")
public class GeoPointDO {

    private Double lat;
    private Double lon;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
