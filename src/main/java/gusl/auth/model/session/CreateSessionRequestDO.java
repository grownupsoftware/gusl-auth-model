package gusl.auth.model.session;

import gusl.auth.model.login.SessionDataDO;
import gusl.core.tostring.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.ZonedDateTime;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CreateSessionRequestDO extends SessionDataDO {
    private String userId;
    private String secretKey;
    private String username;
    private ZonedDateTime userExpiryDate;
    private boolean preSignIn;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
