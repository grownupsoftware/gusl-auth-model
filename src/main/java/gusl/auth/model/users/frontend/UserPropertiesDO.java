package gusl.auth.model.users.frontend;

import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

import java.util.Map;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserPropertiesDO {
    @DocField(description = "Key / Value pair of user personalisation properties")
    Map<String, Object> properties;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
