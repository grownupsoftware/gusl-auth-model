package gusl.auth.model.loggedin;

import gusl.annotations.form.MediaType;
import gusl.auth.AuthType;
import gusl.auth.model.roles.RolesIdsDO;
import gusl.auth.model.session.SessionDO;
import gusl.core.tostring.ToString;
import gusl.model.cohort.CohortIdsDO;
import lombok.*;

import java.awt.*;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Set;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LoggedInUserDO implements ILoggedInUser {
    private String id;
    private String username;
    private String nickname;
    private String avatar;
    private Map<String, Object> properties;
    private SessionDO session;
    private RolesIdsDO roleIds;
    private Set<String> permissions;
    private AuthType authType;
    boolean superUser;
    private ZonedDateTime enabledUntil;
    private ZonedDateTime nextPasswordChange;
    private ReLoginType reLoginType;
    private boolean tOTPRequired;
    private boolean userRegisteredForTOTP;
    private ZonedDateTime lastSignIn;
    private ZonedDateTime lastTotpSetup;
    private String thirdPartyId;
    private String theme;
    private CohortIdsDO allowedCohorts;
    private String currentCohortId;

    private String homePage;

    private MediaType currentMediaType;
    private String orientation;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    @Override
    public boolean canLogin() {
        return true;
    }
}
