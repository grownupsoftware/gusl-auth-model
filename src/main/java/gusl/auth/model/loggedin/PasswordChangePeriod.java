package gusl.auth.model.loggedin;

public enum PasswordChangePeriod {
    NEVER,
    DAILY,
    EVERY_SECOND_DAY,
    WEEKLY,
    FORTNIGHTLY,
    MONTHLY,
    BIMESTRIALLY,
    QUARTERLY
}
