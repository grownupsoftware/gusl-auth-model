package gusl.auth.model.security;

import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ScratchCodeDO {

    private boolean used;

    @DocField(description = "Integer stored as a salted string")
    private String code;

    @DocField(description = "Manually (Obfuscated) - should be cleared once presented to user")
    private String visibleCode;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
