package gusl.auth.model.security;

import gusl.core.tostring.ToString;
import lombok.*;

import java.time.ZonedDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OldSessionTokenRequestDO {
    private String userId;
    private String username;
    private String secretKey;
    private ZonedDateTime expiryDate;
    private ZonedDateTime dateCreated;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
