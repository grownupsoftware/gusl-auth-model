/* Copyright lottomart */
package gusl.auth.model.token;

import gusl.core.annotations.DocField;
import io.jsonwebtoken.Claims;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public abstract class AbstractBaseTokenDO {

    @DocField(description = "Unique ID")
    public String id;

    @DocField(description = "Date the token was created")
    public Date createdDate;

    public abstract Claims getClaims();
}
