package gusl.auth.model.session;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class SessionCacheKey {
    private String userId;
    private String id;

    public static SessionCacheKey of(String userId, String id) {
        return SessionCacheKey.builder()
                .userId(userId)
                .id(id)
                .build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
