package gusl.auth.model.security;

import gusl.core.tostring.ToString;
import lombok.*;

import java.time.ZonedDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ChangePasswordRequestDO {

    private String id;
    private String newPassword;
    private String oldPassword;
    private String username;
    private String adminUser;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
