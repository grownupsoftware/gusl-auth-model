package gusl.auth.jwt;

import gusl.model.exceptions.UnAuthorizedException;
import io.jsonwebtoken.Claims;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface JwtService {

    String COOKIE_NAME = "__gusl_web_v1";

    String USER_TOKEN_NAME = "user-token";
    String ANONYMOUS_TOKEN_NAME = "anon-token";
    String SESSION_TOKEN_NAME = "session-token";
    String ANALYTICS_TOKEN_NAME = "analytics-token";

    String AUTHORIZATION_HEADER = "Authorization";
    String DIGEST_TYPE = "Bearer";

    byte[] generateNewKey();

    String signClaims(byte[] securityKey, Claims claims);

    Claims parseToken(byte[] securityKey, String token) throws UnAuthorizedException;

    byte[] getCustomerSecurityKey();

    byte[] getAnonSecurityKey();

    byte[] getAnalyticsSecurityKey();

    byte[] getSessionSecurityKey();
}
