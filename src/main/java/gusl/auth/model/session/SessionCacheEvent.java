package gusl.auth.model.session;

import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;
import gusl.model.cache.CacheGroup;

public class SessionCacheEvent extends AbstractCacheActionEvent<SessionDO> {

    public SessionCacheEvent() {
    }

    public SessionCacheEvent(CacheAction action, SessionDO entity) {
        super(action, entity);
    }

    @Override
    public CacheGroup getCacheGroup() {
        return CacheGroup.SYSTEM;
    }
}
