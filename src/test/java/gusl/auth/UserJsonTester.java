package gusl.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.auth.model.users.frontend.UserDO;
import gusl.core.json.ObjectMapperFactory;
import lombok.CustomLog;
import org.junit.Test;

import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * @author dhudson
 * @since 08/08/2022
 */
@CustomLog
public class UserJsonTester {

    @Test
    public void userJsonTest() {

        try {
            ObjectMapper mapper = ObjectMapperFactory.getDefaultObjectMapper();

            final ZonedDateTime now = ZonedDateTime.now(ZoneId.of("UTC"));

            UserDO user = new UserDO();
            user.setLastSignIn(now);

            logger.info("{}", ObjectMapperFactory.prettyPrint(user, mapper));
        } catch (Throwable t) {
            logger.warn("Nope .. ", t);
        }
    }
}
