package gusl.auth.model.login.cohorts;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserCohortsDTO {

    @Singular
    private List<CohortDTO> allowedCohorts;
    private CohortDTO currentCohort;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
