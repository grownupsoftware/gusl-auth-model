package gusl.auth.model.security;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class VerifyValidationKeyRequestDO {

    private String token;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
