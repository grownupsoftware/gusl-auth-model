package gusl.auth.cache;

import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.auth.model.login.cohorts.CohortDTO;
import gusl.cache.AbstractIdentifiableStringCache;
import gusl.core.eventbus.OnEvent;
import gusl.core.json.JsonUtils;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.DateFormats;
import gusl.core.utils.Platform;
import gusl.model.Identifiable;
import gusl.model.event.DumpCacheEvent;
import gusl.model.event.LogCacheEvent;
import gusl.model.nodeconfig.NodeDetails;
import gusl.node.converter.ModelConverter;
import gusl.node.eventbus.NodeEventBus;
import gusl.query.InQuery;
import gusl.query.MatchQuery;
import gusl.query.QueryParams;

import javax.inject.Inject;
import javax.inject.Provider;
import java.io.File;
import java.io.IOException;
import java.util.Date;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public abstract class CohortUtilsCacheImpl<T extends Identifiable<String>> extends AbstractIdentifiableStringCache<T> implements CohortUtilsCache {

    public abstract String getDefaultBOCohortId();

    public abstract String getDefaultFECohortId();

    public abstract String getCohortLogo(String cohortId);

    public abstract CohortDTO getCohortDTO(String cohortId);

    @Override
    public void addCohortToQueryParamsForBO(final LoggedInUserDO loggedInUser, QueryParams params) {
        addCohortToQueryParams(loggedInUser, getDefaultBOCohortId(), params);
    }

    @Override
    public void addCohortToQueryParamsForFE(final LoggedInUserDO loggedInUser, QueryParams params) {
        addCohortToQueryParams(loggedInUser, getDefaultFECohortId(), params);
    }

    @Override
    public void addCohortToQueryParams(final LoggedInUserDO loggedInUser, String defaultCohortId, QueryParams params) {
        if (isNull(loggedInUser.getAllowedCohorts())
                || isNull(loggedInUser.getAllowedCohorts().getIds())
                || loggedInUser.getAllowedCohorts().getIds().isEmpty()
                || isNull(params)) {
            return;
        }
        if (isNull(loggedInUser.getCurrentCohortId()) || ALL_USER_ALLOWED_COHORTS.equals(loggedInUser.getCurrentCohortId())) {
            // ALL_USER_ALLOWED_COHORTS => Add all users cohorts

            Object[] values = new Object[loggedInUser.getAllowedCohorts().getIds().size() + 1];
            values[0] = defaultCohortId;

            int index = 1;
            for (String cohort : loggedInUser.getAllowedCohorts().getIds()) {
                values[index++] = cohort;
            }

            params.addIns(InQuery.builder().field("cohortId").values(values).build());
            return;
        }
        if (nonNull(loggedInUser.getCurrentCohortId())) {
            params.addMust(MatchQuery.of("cohortId", loggedInUser.getCurrentCohortId()));
        }
    }

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    @Inject
    private NodeEventBus theEventBus;

    @Inject
    private NodeDetails theNodeDetails;

    @Inject
    protected Provider<ModelConverter> theModelConverterProvider;

    protected ModelConverter getModelConverter() {
        return theModelConverterProvider.get();
    }

    public <IN, OUT> OUT convert(IN in, Class<OUT> klass) {
        return getModelConverter().convert(in, klass);
    }

    protected void fireEvent(Object event) {
        theEventBus.post(event);
    }

    @OnEvent
    public void handleEvent(LogCacheEvent event) {
        logCacheSize();
    }

    public void logCacheSize() {
        logger.info("Cache [{}] No. Records [{}]", this.getClass().getSimpleName(), getCacheSize());
    }

    public void dump() {
        try {
            File dump = new File(Platform.getTmpDirectory()
                    + File.separator
                    + theNodeDetails.getNodeType().name()
                    + "_"
                    + theNodeDetails.getNodeId()
                    + "_"
                    + this.getClass().getSimpleName()
                    + "_"
                    + DateFormats.formatDate("yyyyMMdd_HHmmss", new Date())
                    + ".json");

            JsonUtils.storeObject(getAll(), dump.getAbsolutePath());
        } catch (IOException ex) {
            logger.warn("Failed to dump cache for {}", this.getClass().getSimpleName(), ex);
        }
    }

    @OnEvent
    public void handleDumpEvent(DumpCacheEvent event) {
        dump();
    }
}
