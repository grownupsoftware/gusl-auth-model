package gusl.auth.model.device;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DeviceInfoDO {

    private String uuid;
    private String model;
    private String platform;
    private String manufacturer;
    private boolean isBrowser;
    private String releaseNumber;
    private boolean mobileWeb;
    private boolean desktop;
    private boolean ios;
    private boolean android;
    private String countryId;
    private String cordova;
    private String version;
    private String isVirtual;
    private String serial;
    private String deviceToken;
    private Integer buildId;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
