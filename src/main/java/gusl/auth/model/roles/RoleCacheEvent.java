package gusl.auth.model.roles;

import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;
import gusl.model.cache.CacheGroup;

public class RoleCacheEvent extends AbstractCacheActionEvent<RoleDO> {

    public RoleCacheEvent() {
    }

    public RoleCacheEvent(CacheAction action, RoleDO entity) {
        super(action, entity);
    }

    @Override
    public CacheGroup getCacheGroup() {
        return CacheGroup.SYSTEM;
    }
}
