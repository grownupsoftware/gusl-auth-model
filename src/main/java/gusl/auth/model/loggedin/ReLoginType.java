package gusl.auth.model.loggedin;

public enum ReLoginType {
    NEVER,
    DAILY,
    EVERY_SECOND_DAY,
    WEEKLY,
    FORTNIGHTLY,
    MONTHLY
}
