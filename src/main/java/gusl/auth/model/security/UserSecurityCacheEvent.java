package gusl.auth.model.security;

import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;
import gusl.model.cache.CacheGroup;

public class UserSecurityCacheEvent extends AbstractCacheActionEvent<UserSecurityDO> {

    public UserSecurityCacheEvent() {
    }

    public UserSecurityCacheEvent(CacheAction action, UserSecurityDO entity) {
        super(action, entity);
    }

    @Override
    public CacheGroup getCacheGroup() {
        return CacheGroup.SYSTEM;
    }
}
