package gusl.auth.model.login;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GetUserByUsernameRequestDO {

    private String username;

    public static GetUserByUsernameRequestDO of(String username) {
        return GetUserByUsernameRequestDO.builder()
                .username(username)
                .build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
