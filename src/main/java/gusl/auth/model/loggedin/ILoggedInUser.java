package gusl.auth.model.loggedin;

import gusl.annotations.form.MediaType;
import gusl.auth.model.roles.RolesIdsDO;
import gusl.model.Identifiable;
import gusl.model.cohort.CohortIdsDO;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Set;

public interface ILoggedInUser extends Identifiable<String> {
    boolean canLogin();

    String getId();

    String getUsername();

    String getAvatar();

    Map<String, Object> getProperties();

    RolesIdsDO getRoleIds();

    Set<String> getPermissions();

    boolean isSuperUser();

    void setSuperUser(boolean superuser);

    void setRoleIds(RolesIdsDO roleIds);

    void setPermissions(Set<String> permissions);

    ZonedDateTime getEnabledUntil();

    ZonedDateTime getNextPasswordChange();

    ReLoginType getReLoginType();

    boolean isTOTPRequired();

    void setTOTPRequired(boolean required);

    boolean isUserRegisteredForTOTP();

    void setUserRegisteredForTOTP(boolean registered);

    ZonedDateTime getLastSignIn();

    ZonedDateTime getLastTotpSetup();

    String getThirdPartyId();

    String getTheme();

    String getNickname();

    void setTheme(String theme);

    void setAvatar(String avatar);

    void setNickname(String nickname);

    CohortIdsDO getAllowedCohorts();

    void setAllowedCohorts(CohortIdsDO ids);

    String getCurrentCohortId();

    void setCurrentCohortId(String cohortId);

    void setEnabledUntil(ZonedDateTime dateTime);

    void setNextPasswordChange(ZonedDateTime dateTime);

    void setThirdPartyId(String thirdPartyId);

    String getHomePage();

    void setHomePage(String homePage);

    void setCurrentMediaType(MediaType mediaType);

    void setOrientation(String orientation);

}
