package gusl.auth.model.loggedin;

public interface LoggedInCacheUser {
    boolean canLogin();

    String getId();
}
