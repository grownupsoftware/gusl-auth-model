package gusl.auth.model.roles;

import gusl.auth.model.loggedin.ILoggedInUser;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;

import java.util.Optional;

import static java.util.Objects.isNull;

public class RoleHelper {

    protected final static GUSLLogger logger = GUSLLogManager.getLogger(RoleHelper.class);

    public static final String SUPER_USER_ID = "SUPER_USER";

    public static boolean isSuperUser(ILoggedInUser user) {
        return Optional.ofNullable(user)
                .map(ILoggedInUser::getRoleIds)
                .map(RolesIdsDO::getIds)
                .map(roles -> roles.contains(SUPER_USER_ID))
                .orElse(false);
    }

    public static boolean hasPermission(ILoggedInUser user, String permission) {
        if (isNull(user) || isNull(user.getPermissions())) {
            return false;
        }
        return user.getPermissions().contains(permission);
    }

    public static boolean hasPermissionOrIsSuperUser(ILoggedInUser user, String permission) {
        return isSuperUser(user) || hasPermission(user, permission);
    }

//    public static boolean hasAnyPermissionOrIsSuperUser(ILoggedInUser user, String... permission) {
//        return isSuperUser(user) || !Collections.disjoint(user.getPermissions().getPermissions(), Arrays.asList(permission));
//    }

//    public static boolean isSuperUser(ILoggedInUser user) {
//        if (isNull(user) || isNull(user.getRolesIds()) || isNull(user.getRolesIds().getIds())) {
//            return false;
//        }
//        return user.getRolesIds().getIds().stream().anyMatch(id -> SUPER_USER_ID.equals(id));
//    }

//    public static boolean isSuperUser(AdminUser adminUser) {
//        return Optional.ofNullable(adminUser)
//                .map(AdminUser::getRolesIds)
//                .map(AdminRolesIdsDO::getIds)
//                .map(roles -> roles.contains(SUPER_USER_ID))
//                .orElse(false);
//    }

//    public static boolean isSuperUser(AdminLoggedInUserDO adminLoggedInUserDo) {
//        return adminLoggedInUserDo.getAdminRoles().stream().anyMatch(e -> SUPER_USER_ID.equals(e.getId()));
//    }

//    public static boolean hasPermission(AdminLoggedInUserDO adminLoggedInUserDO, String permission) {
//        return adminLoggedInUserDO.getPermissions().contains(permission);
//    }
//
//    public static boolean hasPermissionOrIsSuperUser(AdminLoggedInUserDO admin, String permission) {
//        return isSuperUser(admin) || hasPermission(admin, permission);
//    }
//
//    public static boolean hasAnyPermissionOrIsSuperUser(AdminLoggedInUserDO admin, String... permission) {
//        return isSuperUser(admin) || !Collections.disjoint(admin.getPermissions(), Arrays.asList(permission));
//    }

}
