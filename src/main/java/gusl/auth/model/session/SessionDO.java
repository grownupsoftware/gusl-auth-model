package gusl.auth.model.session;

import gusl.auth.model.login.SessionDataDO;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import gusl.elastic.model.ESType;
import gusl.elastic.model.ElasticField;
import gusl.elastic.model.WithConfirmation;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import gusl.postgres.model.PostgresIndex;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.ZonedDateTime;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SessionDO extends SessionDataDO implements Session, WithConfirmation {

    public static final int VERSION = 7;

    public static final long DEFAULT_SESSION_TIMEOUT = 600000L;// 10 Minutes
    public static final long PASSWORD_RESET_TIMEOUT = 21600000L;//"6h";
    public static final long SESSION_RENEW_TIME = 30000L; //"30s";
    public static final String SESSION_DATE_FORMAT = "HH:mm:ss.SSS";

    @PostgresField(type = PostgresFieldType.TEXT, primaryKey = true, notNull = true)
    @ElasticField(type = ESType.KEYWORD, populate = true, primaryKey = true)
    private String id;

    @PostgresIndex
    @PostgresField(type = PostgresFieldType.TEXT, notNull = true)
    @ElasticField(type = ESType.KEYWORD)
    private String userId;

    @PostgresIndex
    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String username;

    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateCreated = true, populate = true)
    @ElasticField(type = ESType.DATE, dateCreated = true, populate = true)
    private ZonedDateTime dateCreated;

    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateUpdated = true, populate = true)
    @ElasticField(type = ESType.DATE, dateUpdated = true, populate = true)
    private ZonedDateTime dateUpdated;

    @PostgresField(type = PostgresFieldType.TIMESTAMP)
    @ElasticField(type = ESType.DATE)
    private ZonedDateTime expiryDate;

    @PostgresIndex
    @PostgresField(type = PostgresFieldType.TEXT, notNull = true)
    @ElasticField(type = ESType.KEYWORD)
    private SessionStatus status;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String userToken;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String sessionToken;

    @DocField(description = "Only used for TOTP and change password - cannot have access to anything else")
    @PostgresField(type = PostgresFieldType.BOOLEAN)
    @ElasticField(type = ESType.BOOLEAN)
    private Boolean preSignIn;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    @Override
    public SessionCacheKey getKey() {
        return SessionCacheKey.builder()
                .userId(userId)
                .id(id)
                .build();
    }

}
