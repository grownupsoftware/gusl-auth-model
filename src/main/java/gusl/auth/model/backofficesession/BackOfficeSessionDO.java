package gusl.auth.model.backofficesession;

import gusl.core.tostring.ToString;
import gusl.elastic.model.ESType;
import gusl.elastic.model.ElasticField;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import lombok.*;

import java.time.ZonedDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BackOfficeSessionDO {

    public static final int VERSION = 4;

    @PostgresField(type = PostgresFieldType.TEXT, primaryKey = true, notNull = true, populate = true)
    @ElasticField(type = ESType.KEYWORD, populate = true, primaryKey = true)
    private String id;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String adminUserId;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String email;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String name;

    @ElasticField(type = ESType.DATE, dateCreated = true)
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateCreated = true)
    private ZonedDateTime dateCreated;

    @ElasticField(type = ESType.DATE, dateUpdated = true)
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateUpdated = true)
    private ZonedDateTime dateUpdated;

    @ElasticField(type = ESType.DATE)
    @PostgresField(type = PostgresFieldType.TIMESTAMP)
    private ZonedDateTime sessionEnded;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String remoteIpAddress;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String userAgent;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
