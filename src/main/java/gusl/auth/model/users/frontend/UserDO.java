package gusl.auth.model.users.frontend;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gusl.annotations.form.MediaType;
import gusl.auth.model.loggedin.ILoggedInUser;
import gusl.auth.model.loggedin.PasswordChangePeriod;
import gusl.auth.model.loggedin.ReLoginType;
import gusl.auth.model.loggedin.UserStatus;
import gusl.auth.model.roles.RolesIdsDO;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import gusl.elastic.model.ESType;
import gusl.elastic.model.ElasticField;
import gusl.elastic.model.WithConfirmation;
import gusl.model.Identifiable;
import gusl.model.cohort.CohortIdsDO;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import lombok.*;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Set;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserDO implements Identifiable<String>, ILoggedInUser, WithConfirmation {

    @DocField(description = "Database version - used by Evolutions migrate tool")
    public static final int VERSION = 15;

    @PostgresField(type = PostgresFieldType.TEXT, primaryKey = true, notNull = true, populate = true)
    @ElasticField(type = ESType.KEYWORD, populate = true, primaryKey = true)
    @DocField(description = "Unique id")
    private String id;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "Username or email, used for login. Must be (lowercase) unique")
    private String username;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "Nickname or friendly display name for user")
    private String nickname;

    @ElasticField(type = ESType.DATE, dateCreated = true, populate = true)
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateCreated = true, populate = true)
    @DocField(description = "Date created")
    private ZonedDateTime dateCreated;

    @ElasticField(type = ESType.DATE, dateUpdated = true, populate = true)
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateUpdated = true, populate = true)
    @DocField(description = "Date last modified")
    private ZonedDateTime dateUpdated;

    @PostgresField(type = PostgresFieldType.JSONB)
    @ElasticField(type = ESType.OBJECT)
    @DocField(description = "User personalisation")
    private UserPropertiesDO properties;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "User's avatar")
    private String avatar;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "User status")
    private UserStatus status;

    @DocField(description = "Limited time use. Auto changes to DISABLED")
    @ElasticField(type = ESType.DATE)
    @PostgresField(type = PostgresFieldType.TIMESTAMP)
    private ZonedDateTime enabledUntil;

    @DocField(description = "Date specifying when password should be changed")
    @ElasticField(type = ESType.DATE)
    @PostgresField(type = PostgresFieldType.TIMESTAMP)
    private ZonedDateTime nextPasswordChange;

    @DocField(description = "Last time username and password were presented")
    @ElasticField(type = ESType.DATE)
    @PostgresField(type = PostgresFieldType.TIMESTAMP)
    private ZonedDateTime lastSignIn;

    @DocField(description = "How often password needs to be changed")
    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private PasswordChangePeriod changePeriod;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "Specifies how often username and password must be presented.")
    private ReLoginType reLoginType;

    @ElasticField(type = ESType.DATE)
    @PostgresField(type = PostgresFieldType.TIMESTAMP)
    @DocField(description = "Last date TOTP setup - if null forces UI TOTP setup for user")
    private ZonedDateTime lastTotpSetup;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "Third party id")
    private String thirdPartyId;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "Theme")
    private String theme;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "User's preferred home page")
    private String homePage;

    // Added after login, not persisted
    @DocField(description = "Not persisted, role ids from security record - used in internal cache")
    private RolesIdsDO roleIds;

    @DocField(description = "Allowed access to these cohorts")
    private CohortIdsDO allowedCohorts;

    @DocField(description = "Not persisted, Normalised permissions - used in internal cache")
    private Set<String> permissions;

    @DocField(description = "Not persisted, simple means of identifying if user is superuser - used in internal cache")
    private boolean superUser;

    @DocField(description = "Not persisted, Auth Type requires TOTP")
    private boolean tOTPRequired;

    @DocField(description = "Not persisted, TOTP configured for user")
    private boolean userRegisteredForTOTP;

    @DocField(description = "Current selected cohort")
    private String currentCohortId;

    private MediaType currentMediaType;

    private String orientation;



    @Override
    public String toString() {
        return ToString.toString(this);
    }

    @Override
    public boolean canLogin() {
        return nonNull(status) && status.canLogin();
    }

    @Override
    @JsonIgnore
    public Map<String, Object> getProperties() {
        return isNull(properties) ? null : properties.getProperties();
    }
}
