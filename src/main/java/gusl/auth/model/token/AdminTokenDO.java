/* Copyright lottomart */
package gusl.auth.model.token;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "Data object of JWT used for admin users")
public class AdminTokenDO extends AbstractBaseTokenDO {

    @DocField(description = "User Name")
    public String username;

    @DocField(description = "Expiry date time for this token")
    private Date expiryDate;

    @Override
    public Claims getClaims() {
        Claims claims = Jwts.claims().setSubject(this.username);
        claims.put("id", getId() + "");
        claims.setIssuedAt(getCreatedDate());
        claims.setId(String.valueOf(getId()));
        claims.setExpiration(expiryDate);
        claims.setIssuedAt(getCreatedDate());
        return claims;
    }

    public static AdminTokenDO parseClaims(Claims claims) {
        AdminTokenDO adminTokenDo = new AdminTokenDO();
        adminTokenDo.setUsername(claims.getSubject());
        adminTokenDo.setId(claims.getId());
        adminTokenDo.setExpiryDate(claims.getExpiration());
        adminTokenDo.setCreatedDate(claims.getIssuedAt());

        return adminTokenDo;

    }

}
