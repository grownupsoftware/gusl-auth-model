package gusl.auth.cache;

import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
@Builder
public class CohortLookupBO {

    @UiField(type = FieldType.text)
    private String id;

    @UiField(type = FieldType.text)
    private String name;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
