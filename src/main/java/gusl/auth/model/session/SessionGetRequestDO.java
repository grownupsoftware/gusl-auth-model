package gusl.auth.model.session;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SessionGetRequestDO {

    private String bettorId;
    private String id;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
