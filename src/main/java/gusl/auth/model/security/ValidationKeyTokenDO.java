package gusl.auth.model.security;

import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.*;

import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ValidationKeyTokenDO {

    @DocField(description = "Unique ID")
    public String id;

    @DocField(description = "Date the token was created")
    public Date createdDate;

    private String userSecurityId;

    private String email;

    // must match secretKey on UserSecurityDO otw record changed
    private String userSecretKey;

    private Date expiryDate;

    public Claims getClaims() {
        Claims claims = Jwts.claims().setSubject(email);
        claims.setIssuedAt(getCreatedDate());
        claims.setId(String.valueOf(getId()));
        claims.setExpiration(expiryDate);
        claims.put("userSecurityId", userSecurityId);
        claims.put("userSecretKey", userSecretKey);
        return claims;
    }

    public static ValidationKeyTokenDO parseClaims(Claims claims) {
        return ValidationKeyTokenDO.builder()
                .id(claims.getId())
                .email(claims.getSubject())
                .expiryDate(claims.getExpiration())
                .userSecurityId((String) claims.get("userSecurityId"))
                .userSecretKey((String) claims.get("userSecretKey"))
                .createdDate(claims.getIssuedAt())
                .build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
