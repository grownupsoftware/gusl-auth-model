package gusl.auth.model.geo;

import gusl.core.networking.CIDR;
import gusl.core.tostring.ToString;
import lombok.*;

import java.net.InetAddress;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GeoFenceResponse {

    private CIDR cidr;
    private InetAddress address;
    private String hostname;
    private String countryCode;
    private IpGeoLocationDataDO locationData;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
