package gusl.auth.model.security;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OldSessionTokenResponseDO {

    private String oldUserToken;
    private String oldSessionToken;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
