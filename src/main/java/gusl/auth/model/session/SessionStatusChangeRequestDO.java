package gusl.auth.model.session;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class SessionStatusChangeRequestDO {

    private String adminUserEmail;
    private String bettorId;
    private String id;
    private SessionStatus newStatus;

}
