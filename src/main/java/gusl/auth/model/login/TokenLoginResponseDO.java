package gusl.auth.model.login;

import gusl.auth.model.login.cohorts.UserCohortsDTO;
import gusl.auth.model.roles.PermissionsDO;
import gusl.core.tostring.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.ZonedDateTime;
import java.util.Map;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TokenLoginResponseDO {
    private String id;
    private String username;
    private String nickname;
    private String avatar;
    private String thirdPartyId;
    private Map<String, Object> properties;
    private PermissionsDO permissions;

    // below is optional - only set when new tokens available
    private String userToken;
    private String sessionToken;
    private String homePage;
    private String theme;
    private UserCohortsDTO userCohorts;
    private ZonedDateTime themeLastUpdated;

    private Boolean hasPowerOfAttorney;

    private boolean passwordChangeRequired;

    private String timezonePreference;
    private boolean acceptTandCRequired;
    private String status;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
