package gusl.auth.model.session;

import gusl.auth.model.device.DeviceInfoDO;
import gusl.auth.model.device.UtmDO;
import gusl.auth.model.geo.DeviceGeoLocationDataDO;
import gusl.auth.model.geo.IpGeoLocationDataDO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class SessionDTO {

    private long DEFAULT_SESSION_TIMEOUT;

    private long PASSWORD_RESET_TIMEOUT;

    private long SESSION_RENEW_TIME;

    private String SESSION_DATE_FORMAT;

    private String sessionId;

    private String bettorId;

    private Date dateCreated;

    private Date expiryDate;

    private DeviceInfoDO deviceInfo;

    private DeviceGeoLocationDataDO deviceLocationData;

    private IpGeoLocationDataDO ipLocationData;

    private Date lastStatusChangeDate;

    private SessionStatus status;

    private UtmDO utm;

}
