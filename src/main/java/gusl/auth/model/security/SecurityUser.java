package gusl.auth.model.security;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import gusl.auth.model.loggedin.ILoggedInUser;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.ws.rs.core.SecurityContext;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class SecurityUser {
    private SecurityContext securityContext;
    private GoogleIdToken.Payload payload;
    private ILoggedInUser loggedInUser;

    public boolean hasExpired() {
        return ((payload.getExpirationTimeSeconds() * 1000) <= System.currentTimeMillis());
    }

}
