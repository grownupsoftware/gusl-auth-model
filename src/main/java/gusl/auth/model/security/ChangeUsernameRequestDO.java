package gusl.auth.model.security;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ChangeUsernameRequestDO {

    private String id;
    private String oldUsername;
    private String newUsername;
    private String adminUser;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
