package gusl.auth.model.session;

public enum SessionStatus {
    ACTIVE,
    BLOCKED,
    LOGGED_OUT,
    EXPIRED;

    public boolean isActive() {
        return this == ACTIVE;
    }

    public boolean canRemoveFromCache() {
        return this == BLOCKED
                || this == LOGGED_OUT
                || this == EXPIRED;
    }

}
