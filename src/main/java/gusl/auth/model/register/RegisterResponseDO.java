package gusl.auth.model.register;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class RegisterResponseDO {

    // work in progress
    private String id;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
