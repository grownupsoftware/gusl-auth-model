package gusl.auth.model.register;

import gusl.auth.model.device.DeviceInfoDO;
import gusl.auth.model.device.UtmDO;
import gusl.auth.model.geo.DeviceGeoLocationDataDO;
import gusl.auth.model.geo.IpGeoLocationDataDO;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringIgnore;
import lombok.*;

import java.util.Map;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class RegisterRequestDO {

    private String username;

    @ToStringIgnore
    private String password;

    private Map<String, String> headers;
    private String clientIp;
    private IpGeoLocationDataDO ipLocationData;
    private DeviceGeoLocationDataDO deviceLocationData;
    private DeviceInfoDO deviceInfo;
    private UtmDO utm;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
