package gusl.auth.model.login;

import gusl.auth.model.login.cohorts.UserCohortsDTO;
import gusl.auth.model.roles.PermissionsDO;
import gusl.core.tostring.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.ZonedDateTime;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SignInResponseDO {
    private String id;
    private String username;
    private String sessionToken;
    private String userToken;
    private String nickname;
    private String avatar;
    private boolean passwordChangeRequired;
    private PermissionsDO permissions;
    private String homePage;
    private String theme;
    private UserCohortsDTO userCohorts;

    private ZonedDateTime themeLastUpdated;

    private Boolean hasPowerOfAttorney;

    private String timezonePreference;
    private boolean acceptTandCRequired;
    private String status;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
