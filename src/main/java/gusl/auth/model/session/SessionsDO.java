package gusl.auth.model.session;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class SessionsDO {

    @ToStringCount
    @Singular
    private List<SessionDO> sessions;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
