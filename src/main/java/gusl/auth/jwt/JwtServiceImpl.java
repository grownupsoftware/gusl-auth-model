package gusl.auth.jwt;

import gusl.auth.errors.AuthenticationErrors;
import gusl.core.json.JsonUtils;
import gusl.model.exceptions.UnAuthorizedException;
import gusl.model.nodeconfig.JwtSecurityConfig;
import gusl.model.nodeconfig.NodeConfig;
import gusl.node.properties.GUSLConfigurable;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import java.security.Key;
import java.util.Base64;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Service
@CustomLog
public class JwtServiceImpl implements JwtService, GUSLConfigurable {

    private static final SignatureAlgorithm THE_SIGNATURE_ALGORITHM = SignatureAlgorithm.HS512;

    private byte[] theCustomerSecurityKey;

    private byte[] theAnonSecurityKey;

    private byte[] theAnalyticsSecurityKey;

    private byte[] theSessionSecurityKey;

    public void configure(NodeConfig config) {
        JwtSecurityConfig jwtConfig = config.getJwtSecurityConfig();
        if (isNull(jwtConfig)) {
            return;
        }

        logger.debug("jwtConfig: {}", JsonUtils.prettyPrint(jwtConfig));

        if (nonNull(jwtConfig.getCustomerSecurityKey())) {
            theCustomerSecurityKey = Base64.getDecoder().decode(jwtConfig.getCustomerSecurityKey());
        } else {
            logger.warn("Generating a new customer security key");
            theCustomerSecurityKey = generateNewKey();
        }
        if (nonNull(jwtConfig.getAnonSecurityKey())) {
            theAnonSecurityKey = Base64.getDecoder().decode(jwtConfig.getAnonSecurityKey());
        } else {
            logger.warn("Generating a new anon security key");
            theAnonSecurityKey = generateNewKey();
        }

        if (nonNull(jwtConfig.getAnalyticsSecurityKey())) {
            theAnalyticsSecurityKey = Base64.getDecoder().decode(jwtConfig.getAnalyticsSecurityKey());
        } else {
            logger.warn("Generating a new analytics security key");
            theAnalyticsSecurityKey = generateNewKey();
        }
        if (nonNull(jwtConfig.getSessionSecurityKey())) {
            theSessionSecurityKey = Base64.getDecoder().decode(jwtConfig.getSessionSecurityKey());
        } else {
            logger.warn("Generating a new session security key");
            theSessionSecurityKey = generateNewKey();
        }
    }

    @Override
    public byte[] generateNewKey() {
        Key key = MacProvider.generateKey(SignatureAlgorithm.HS256);
        return key.getEncoded();
    }

    @Override
    public String signClaims(byte[] securityKey, Claims claims) {
        return Jwts.builder()
                .setClaims(claims)
                .signWith(THE_SIGNATURE_ALGORITHM, securityKey)
                .compact();
    }

    @Override
    public Claims parseToken(byte[] securityKey, String token) {
        try {
            return Jwts.parser()
                    .setSigningKey(securityKey)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (JwtException | ClassCastException ex) {
            logger.warn("Failed to parse token: [{}] Error: {}", trimToken(token), ex.getMessage());
            logger.debug("Failed to parse token: [{}] Error: {}", trimToken(token), ex.getMessage(), ex);
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }
    }

    private String trimToken(String token) {
        if (isNull(token)) {
            return "";
        }
        if (token.length() < 10) {
            return token;
        }
        return token.substring(0, 5) + "..." + token.substring(token.length() - 5);
    }

    @Override
    public byte[] getCustomerSecurityKey() {
        return theCustomerSecurityKey;
    }

    @Override
    public byte[] getAnonSecurityKey() {
        return theAnonSecurityKey;
    }

    @Override
    public byte[] getAnalyticsSecurityKey() {
        return theAnalyticsSecurityKey;
    }

    @Override
    public byte[] getSessionSecurityKey() {
        return theSessionSecurityKey;
    }
}
