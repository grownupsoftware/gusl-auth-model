package gusl.auth.model.session;

import gusl.auth.model.device.DeviceInfoDO;
import gusl.auth.model.device.UtmDO;
import gusl.auth.model.geo.DeviceGeoLocationDataDO;
import gusl.auth.model.geo.IpGeoLocationDataDO;
import gusl.model.IdentifiableKey;

import java.time.ZonedDateTime;

public interface Session extends IdentifiableKey<SessionCacheKey> {

    SessionStatus getStatus();

    String getId();

    String getUserId();

    ZonedDateTime getDateCreated();

    ZonedDateTime getExpiryDate();

    DeviceInfoDO getDeviceInfo();

    DeviceGeoLocationDataDO getDeviceLocationData();

    IpGeoLocationDataDO getIpLocationData();

    ZonedDateTime getDateUpdated();

    UtmDO getUtm();

    Boolean getPreSignIn();

    String getUserToken();

    String getSessionToken();

}
