package gusl.auth.model.login;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SignInRequestDO extends SessionDataDO {
    @NotBlank
    private String username;

    @ToStringIgnore
    @NotBlank
    private String password;

    @ToStringIgnore
    private String totp;

    private boolean preSignIn;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
