package gusl.auth.model.geofence;

import gusl.annotations.form.page.AbstractPageResponse;
import gusl.annotations.form.page.PageResponse;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class PagedGeoFenceDO extends AbstractPageResponse implements PageResponse<GeoFenceDO> {

    @ToStringCount
    private List<GeoFenceDO> content;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}

