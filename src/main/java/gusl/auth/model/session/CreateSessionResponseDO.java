package gusl.auth.model.session;

import gusl.core.tostring.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CreateSessionResponseDO {

    private String id;
    private SessionDO session;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
